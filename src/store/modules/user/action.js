import {LOGIN} from './action-types';

export const userLogin = (token) =>({
    type: LOGIN,
    token
})
