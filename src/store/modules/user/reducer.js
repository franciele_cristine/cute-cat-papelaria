import {LOGIN} from './action-types';

const defaultState = {
    token: localStorage.getItem("authToken") || "",
  };
  

const userReducer = (state = defaultState, action) =>{
    switch (action.type) {
        case LOGIN:
            const {token} = action;
            return {...state, token: token}
    
        default:
            return state;
    }
}

export default userReducer