import api from '../../../helpers/api';
import {userLogin} from './action';

export const userLoginThunk = (data) => (dispatch) =>{
    api.post('/login', data)
        .then(res => (
            localStorage.setItem("authToken", res.data.accessToken),
            dispatch(userLogin(res.data.accessToken))
            ))
        .catch(err => console.log(err))
        
}