import {userRegister} from './actions';
import api from '../../../helpers/api';

export const userRegisterThunk = (data) => (dispatch) =>{
    api.post('/register', {...data})
        .then(res => dispatch(userRegister(res.data)))
        .catch(err => console.log(err))
}