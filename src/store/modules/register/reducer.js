import {REGISTER} from './action-types';

const registerReducer = (state = [], action) =>{
    switch (action.type) {
        case REGISTER:
            const {data} = action;
            return state = [...state, data]
    
        default:
            return state
    }
}

export default registerReducer