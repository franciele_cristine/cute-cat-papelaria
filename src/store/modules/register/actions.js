import {REGISTER} from './action-types';

export const userRegister = (data) =>({
    type: REGISTER,
    data
})