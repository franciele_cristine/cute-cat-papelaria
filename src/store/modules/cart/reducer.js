import {ADD, REMOVE} from './actions-types';

const cartReducer = (state = [], action) =>{
    switch (action.type) {
        case ADD:
            const {product} = action;
            return state = [...state, product]
        case REMOVE:
            const {list} = action;
            return list
        default:
            return state;
    }
}

export default cartReducer