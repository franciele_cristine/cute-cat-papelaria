import {addToCart, removeFromCart} from './actions';

export const addToCartThunk = (product) => (dispatch) =>{
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    const exist = cart.find((item) => item.id === product.id)

    if(exist === undefined){
        product.qtd = 1
        cart.push(product)
    }else{
        return product.qtd += 1
    }
       
    localStorage.setItem('cart', JSON.stringify(cart))
    dispatch(addToCart(product));
}

export const removeFromCartThunk = (id) => (dispatch, getState) =>{
    const {cart} = getState();
    const list = cart.filter(product => product.id !== id)
    localStorage.setItem('cart', JSON.stringify(list));
    dispatch(removeFromCart(list))
}