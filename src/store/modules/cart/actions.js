import {ADD, REMOVE} from './actions-types';

export const addToCart = (product) => ({
    type: ADD,
    product
})

export const removeFromCart = (list) => ({
    type: REMOVE,
    list
})