import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import productsReducer from './modules/products/reducer';
import registerReducer from './modules/register/reducer';
import userReducer from './modules/user/reducer';
import cartReducer from './modules/cart/reducer';
import thunk from 'redux-thunk';


const reducers = combineReducers({
    products: productsReducer,
    user: userReducer,
    register: registerReducer,
    cart: cartReducer
})
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

export default store