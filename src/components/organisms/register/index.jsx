import * as yup from 'yup';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {states} from '../../../helpers/states';
import {useDispatch} from 'react-redux';
import {userRegisterThunk} from '../../../store/modules/register/thunk';
import {useHistory} from 'react-router-dom';

const Register = () =>{
    const dispatch = useDispatch();
    const history = useHistory();
    const schema = yup.object().shape({
        name: yup.string().required("Campo Obrigatório").min(4, "Mínimo de 4 caracteres"),
        email: yup.string().email("E-mail inválido").required("Campo Obrigatório"),
        password: yup.string().required("Campo Obrigatório").min(8, "Mínimo de 8 caracteres"),
        password_confirm: yup.string().required("Campo Obrigatório").oneOf([yup.ref('password')], "Confirmação de senha deve ser igual a senha"),
        cpf: yup.string().required("Campo Obrigatório").matches(/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/, "CPF inválido"),
        address: yup.string().required("Campo Obrigatório"),
        cep: yup.string().matches(/[0-9]{5}-[0-9]{3}/, "CEP Inválido").required("Campo Obrigatório"),
        city: yup.string().required("Campo Obrigatório"),
        state: yup.string().required("Campo Obrigatório")

    })
    const{handleSubmit, errors, register} = useForm({
        resolver: yupResolver(schema)
    })

   const handleForm = (data, e) =>{
       dispatch(userRegisterThunk(data))
       e.target.reset()
       history.push("/login")
   }
    return(
        <div >
        <h1>Register</h1>
        <form onSubmit={handleSubmit(handleForm)}style={{display: 'flex', flexDirection: "column"}}>
            <input placeholder="Nome" name="name" ref={register}/>
            {errors.name && <span>{errors.name.message}</span>}
            <input placeholder="e-mail" name="email" ref={register}/>
            {errors.email && <span>{errors.email.message}</span>}
            <input placeholder="senha" name="password" ref={register}/>
            {errors.password && <span>{errors.password.message}</span>}
            <input placeholder="confirmar senha" name="password_confirm" ref={register}/>
            {errors.password_confirm && <span>{errors.password_confirm.message}</span>}
            <input placeholder="CPF" name="cpf" ref={register}/>
            {errors.cpf && <span>{errors.cpf.message}</span>}
            <input placeholder="Endereço" name="address" ref={register}/>
            {errors.address && <span>{errors.address.message}</span>}
            <input placeholder="CEP" name="cep" ref={register}/>
            {errors.cep && <span>{errors.cep.message}</span>}
            <input placeholder="Cidade" name="city" ref={register}/>
            {errors.city && <span>{errors.city.message}</span>}
            <select placeholder="Estado" name="state" ref={register}>
                {states.map((state, index) =>{
                    return(
                        <option key={index} value={state}>{state}</option>
                    )
                })}  
            </select>
            {errors.state && <span>{errors.state.message}</span>}
            <button type="submit">Cadastrar</button>
        </form>
        </div>
    )
}
export default Register