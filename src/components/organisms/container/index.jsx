import {FlexContainer} from './style';

const Container = (props) => {
    return(
        <FlexContainer>
            {props.children}
        </FlexContainer>
    )
}

export default Container