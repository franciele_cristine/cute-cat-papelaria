import styled from 'styled-components';

export const FlexContainer = styled.div `
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    margin: 2% auto;
    width: 80vw;
`