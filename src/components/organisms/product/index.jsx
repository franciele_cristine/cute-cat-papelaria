import ProductOptions from '../../molecules/product-options';
import {Container} from './style';
import Column from '../../atoms/column';
import ProductImage from '../../atoms/productImage';
import ProductHeader from '../../molecules/product-header';
import LongDescription from '../../atoms/long-description';
import Information from '../../molecules/information';
import {addToCartThunk} from '../../../store/modules/cart/thunk';
import {useDispatch} from 'react-redux';
const Product = ({options, product}) =>{
    const dispatch = useDispatch();
    return(
        <Container>
            <Column width={'30vw'}>
                <ProductImage product={product}/>
            </Column>
            <Column width={'40vw'}>
                <ProductHeader title={product.name} brand={product.brand}/>
                <LongDescription>{product.description_long}</LongDescription>
                <ProductOptions options={options}/>
                <Information price={`R$ ${product.price.toFixed(2)}`} size="big" onClick={() => dispatch(addToCartThunk(product))}/>
            </Column>
        </Container>
    )
}

export default Product