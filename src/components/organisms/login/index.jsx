import {Container} from './style';
import Button from '../../atoms/button';
import * as yup from 'yup';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {userLoginThunk} from '../../../store/modules/user/thunk';
import {useDispatch} from 'react-redux';
import FormLink from '../../atoms/form-links';
import FormInput from '../../atoms/form-inputs';


const Login = () =>{
    const dispatch = useDispatch()
    const schema = yup.object().shape({
        email: yup.string().email("Formato inválido"),
        password: yup.string().required("Campo obrigatório").min(8, "Mínimo de 8 caracteres")
    })
    const {handleSubmit, register, errors} = useForm({
        resolver: yupResolver(schema)
    })

    const handleForm = (data) =>{
        dispatch(userLoginThunk(data))
    }
    return(
        <Container onSubmit={handleSubmit(handleForm)}>
            <input placeholder="e-mail" name="email" ref={register}/>
            {errors.email && <span>{errors.email.message}</span>}
            <input placeholder="senha" name="password" ref={register}/>
            {errors.password && <span>{errors.password.message}</span>}
            <Button type="submit">Enviar</Button>
            <FormLink route={'/register'}>Não tenho Conta</FormLink>            
        </Container>
    )
}

export default Login