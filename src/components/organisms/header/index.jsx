import CartLink from '../../molecules/cart-link';
import LoginLink from '../../molecules/login-link';
import Search from '../../molecules/search';
import HomeLink from '../../molecules/home-link';
import {Container} from './styled';

const Header = () =>{
    return(
    <Container>
        <HomeLink/>
        <LoginLink/>
        <Search/>
        <CartLink/>
    </Container>
    )
}

export default Header