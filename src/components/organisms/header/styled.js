import styled from 'styled-components';

export const Container = styled.div `
    background-color: #FF7BA3;
    height: 35px;
    width: 99.1vw;
    display: flex;
    justify-content: space-between;
    align-items: center;
`