import Banner from '../../atoms/banner';
import {Carousel} from 'antd';
import {banners} from '../../../helpers/banners';
import 'antd/dist/antd.css';

const Slide = () =>{
    return(
        <Carousel autoplay>
            {banners.map((banner, index) =>{
                return(
                    <Banner image={banner} key={index}/>
                )
            })}
        </Carousel>
    )
}

export default Slide