import {useSelector, useDispatch} from 'react-redux';
import {removeFromCartThunk} from '../../../store/modules/cart/thunk';
import {Link, useHistory} from 'react-router-dom';

const Cart = () =>{
    const dispatch = useDispatch();
    const history = useHistory();
    const cart = useSelector(state => state.cart);
    const total = cart.reduce((acc, product) => (acc + product.price), 0)
    const totalPrice = (qtd, price) =>{
        return (qtd * price).toFixed(2)
    }
    return(
        <>
        <h1>Carrinho</h1>
        <h2>Total - R${total.toFixed(2)}</h2>
        <Link to='/'>Adicionar mais produtos</Link>
        <button onClick={() => history.push('/checkout')}>finalizar compra</button>
            {cart.map((product, index) =>{
                return(
                    <div key={index}><img style={{width: "65px"}} 
                    src={product.url} alt={product.name}/>{product.name} - 
                    {product.qtd} - R${product.price.toFixed(2)} - {`R$${totalPrice(product.qtd, product.price)}`}
                    <button onClick={() => dispatch(removeFromCartThunk(product.id))}>remover</button>
                    </div>
                )
        })}
        </>
    )
}

export default Cart