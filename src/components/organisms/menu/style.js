import styled from 'styled-components';

export const Container = styled.div ` 
    height: 100vh;
    width: 15vw;
    background-color: #9EE8FB;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    font-weight: 900;
    position: relative;
    float: left;
`