import Links from '../../atoms/links';
import {categories} from  '../../../helpers/categories';
import {Container} from './style';

const Menu = () =>{
    return(
        <Container>
            {categories.map((category, index) => {
                return(
                    <Links name={category} link={`/${category}`} key={index}/>
                )
            })}
        </Container>
    )
}

export default Menu