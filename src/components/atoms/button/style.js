import styled from 'styled-components';

export const SmallButton = styled.button `
    border: none;
    background-color: #9EE8FB;
    color: #fff;
    border-radius: 10px;
    width: 80px;
    font-weight: 900;
    margin: -10px auto;
    margin-bottom: 5%;
    &:hover{
        background-color: #20cbf7;
    }
`
export const BigButton = styled(SmallButton) `
    width: 120px;
    height: 40px;
`