import {BigButton, SmallButton} from './style';

const Buttons = (props) =>{
    return(
        props.size === "small" ? <SmallButton onClick={props.onClick}>{props.children}</SmallButton> : 
        <BigButton onClick={props.onClick}>{props.children}</BigButton>
    )
}

export default Buttons