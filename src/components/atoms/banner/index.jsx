import { Container } from "./style"

const Banner = ({image}) => {

    return(
        <Container style={{backgroundImage: `url(${image})`}}>
        </Container>
    )
}

export default Banner