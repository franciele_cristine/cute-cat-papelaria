import {Image} from './style';

const ProductImage = ({product}) =>{
    return(
        <Image src={product.url} alt={product.name}/>
    )
}

export default ProductImage