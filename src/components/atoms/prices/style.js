import styled from 'styled-components';

export const Price = styled.p `
    color: #9EE8FB;
    text-align: center;
    font-size: 1.1rem;
    font-weight: 900;
`