import {Price} from './style';

const Prices = (props) => {
    return(
        <Price>
            {props.children}
        </Price>
    )
}

export default Prices