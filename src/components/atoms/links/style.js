import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const StyledLink = styled(Link) `
    color: #fff;
    font-size: 1rem;
    text-decoration: none;
`