import {Link} from 'react-router-dom';
import {StyledLink} from './style';

const Links = (props) =>{
    return(
        <StyledLink to={props.link}>{props.name}</StyledLink>
    )
}

export default Links