import {Container} from './style';

const LongDescription = (props) =>{
    return(
        <Container>
            {props.children}
        </Container>
    )
}

export default LongDescription
