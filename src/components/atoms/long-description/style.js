import styled from 'styled-components';

export const Container = styled.div `
    font-size: 1.3rem;
    color: #707070;
    width: 40vw;
`