import styled from 'styled-components';

export const Container = styled.div `
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-content: space-evenly;
    justify-content: space-evenly;
`