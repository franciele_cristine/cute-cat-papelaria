import {Container} from './style';

const Column = (props) =>{
    return(
        <Container style={{width: `${props.width}`}}>
            {props.children}
        </Container>
    )
}

export default Column