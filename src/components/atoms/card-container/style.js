import styled from 'styled-components';

export const Container = styled.div `
    display: flex;
    flex-direction: column;
    width: 200px;
    height: 300px;
    box-shadow: 3px 3px 5px #c7c6c5;
    margin-bottom: 5%;
`