import {Container} from './style';

const CardContainer = (props) =>{
    return(
        <Container>
            {props.children}
        </Container>
    )
}

export default CardContainer