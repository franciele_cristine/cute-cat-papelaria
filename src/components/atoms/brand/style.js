import styled from 'styled-components';

export const StyledBrand = styled.p `
    color: #707070;
    font-size: 1rem;
    margin-top: 0;
`