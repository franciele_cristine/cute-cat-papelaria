import {StyledBrand} from './style';

const Brand = (props) => {
    return(
        <StyledBrand>{props.children}</StyledBrand>
    )
}

export default Brand