import styled from 'styled-components';

export const Description = styled.p `
    color: #707070;
    width: 195px;
    margin: auto;
    font-size: 0.8rem;
    text-align: center;
`