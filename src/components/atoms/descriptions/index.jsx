import {Description} from './style';

const Descriptions = (props) => {
    return(
        <Description>{props.children}</Description>
    )
}

export default Descriptions