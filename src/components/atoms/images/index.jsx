import {Image} from './style';

const Images = ({url}) =>{
    return(
        <Image style={{backgroundImage: `url(${url})`}}/>
    )
}

export default Images