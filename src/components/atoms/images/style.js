import styled from 'styled-components';

export const Image = styled.div `
    width: 200px;
    height: 150px;
    background-size: contain;
    background-repeat: no-repeat;
`