import styled from 'styled-components';

export const InputSearch = styled.input ` 
    border-radius: 50px;
    width: 180px;
    height: 20px;
    border: none;
    padding-left: 5%;
    margin-top: 5%;
`
export const Input = styled.input `
    border-radius: 5px;
    border: 1px solid #c4c4c4;
    width: 50px;
    padding-left: 2%;
`