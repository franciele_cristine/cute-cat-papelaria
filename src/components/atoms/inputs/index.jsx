import {InputSearch, Input} from './style';

const Inputs = ({type, placeholder}) =>{
    return(
        type === "search" ? <InputSearch placeholder={placeholder}/> : <Input placeholder={placeholder} type={type}/>
    )
}

export default Inputs