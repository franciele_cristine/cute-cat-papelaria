import styled from 'styled-components';

export const Icon = styled.div `
    color: #fff;
    font-size: 1.4rem;
`