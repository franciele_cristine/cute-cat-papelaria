import {Icon} from './style';

const Icons = (props) => {
    return(
        <Icon>{props.children}</Icon>
    )
}

export default Icons