import {StyledOption} from './style';

const Option = ({value, label}) =>{
    return(
        <StyledOption value={value}>{label}</StyledOption>
    )
}

export default Option