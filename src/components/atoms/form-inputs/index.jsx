import {Input} from './style';

const FormInputs = ({placeholder, type}) =>{
    return <Input placeholder={placeholder} type={type}/>
}

export default FormInputs