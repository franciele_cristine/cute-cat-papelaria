import styled from 'styled-components';

export const Input = styled.input `
    border: 1px solid #707070;
    border-radius: 28px;
    height: 30px;
    padding-left: 2%;
    width: 30vw;
    color: #707070;
`