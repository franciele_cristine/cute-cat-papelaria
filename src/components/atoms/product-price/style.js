import styled from 'styled-components';

export const Container = styled.div `
    color: #FF7BA3;
    font-weight: 900;
    font-size: 2rem;
`