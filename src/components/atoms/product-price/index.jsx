import {Container} from './style';

const ProductPrice = (props) =>{
    return(
        <Container>
            {props.children}
        </Container>
    )
}

export default ProductPrice