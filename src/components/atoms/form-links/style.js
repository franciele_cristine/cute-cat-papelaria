import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const StyledLink = styled(Link) `
    color: #707070;
    text-align: center;
    font-size: 1.4rem;
`