import {StyledLink} from './style';
import {Link} from 'react-router-dom';

const FormLinks = (props) =>{
    return <StyledLink to={props.route}>{props.children}</StyledLink>
}

export default FormLinks