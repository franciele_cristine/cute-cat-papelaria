import {StyledSelect} from './style';

const Select = (props) =>{
    return(
        <StyledSelect>
            {props.children}
        </StyledSelect>
    )
}

export default Select