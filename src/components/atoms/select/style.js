import styled from 'styled-components';

export const StyledSelect = styled.select`
    border: 1px solid #c4c4c4;
    width: 250px;
    background-color: #fff;
    border-radius: 50px;
`