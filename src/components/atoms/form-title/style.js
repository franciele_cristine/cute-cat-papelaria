import styled from 'styled-components';

export const Title = styled.h1 `
    color: #FF7BA3;
    font-weight: 900;
    font-size: 2rem;
`