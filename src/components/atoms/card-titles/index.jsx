import {Title} from './style';

const Titles = (props) =>{
    return(
        <Title>{props.children}</Title>
    )
    
}

export default Titles