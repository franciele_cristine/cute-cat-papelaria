import styled from 'styled-components';

export const Title = styled.h1 `
    color: #FF7BA3;
    font-size: 1rem;
    font-weight: 900;
    width: 195px;
    margin: auto;
    text-align: center;
`