import ProductImage from '../productImage';
import {Title} from './style';

const ProductTitle = (props) =>{
    return (
        <Title>{props.children}</Title>
    )
}

export default ProductTitle