import Option from '../../atoms/option';
import Select from '../../atoms/select';

const Selector = ({options}) =>{
    return(
        <Select>
            {options.map((option) => {
                return(
                    <Option key={option.id} value={option.value} label={option.value}/>
                )
            })}
        </Select>
    )
}

export default Selector