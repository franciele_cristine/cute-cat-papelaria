import Icon from '../../atoms/icons';
import Links from '../../atoms/links';
import {AiOutlineHome} from 'react-icons/ai';
import {Container} from './style';

const HomeLink = () =>{
    return(
        <Container>
            <Links link={'/'} name={<Icon><AiOutlineHome/></Icon>}/>
        </Container>
        
    )
}

export default HomeLink