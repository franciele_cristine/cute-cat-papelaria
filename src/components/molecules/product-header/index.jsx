import {Container} from './style';
import ProductTitle from '../../atoms/product-title';
import Brand from '../../atoms/brand';

const ProductHeader = ({title, brand}) =>{
    return(
        <Container>
            <ProductTitle>{title}</ProductTitle>
            <Brand>{brand}</Brand>
        </Container>
    )
}

export default ProductHeader