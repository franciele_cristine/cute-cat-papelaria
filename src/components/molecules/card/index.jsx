import Buttons from '../../atoms/button';
import CardContainer from '../../atoms/card-container';
import Descriptions from '../../atoms/descriptions';
import Images from '../../atoms/images';
import Prices from '../../atoms/prices';
import Titles from '../../atoms/card-titles';
import {useHistory} from 'react-router-dom';

const Card = ({product}) => {
    const history = useHistory();

    return (
        <CardContainer>
            <Images url={product.url}/>
            <Titles>{product.name}</Titles>
            <Descriptions>{product.description_short}</Descriptions>
            <Prices>R$ {product.price.toFixed(2)}</Prices>
            <Buttons size="small" onClick={() => history.push(`/products/${product.name}`)}>ver +</Buttons>
        </CardContainer>
    )
}

export default Card