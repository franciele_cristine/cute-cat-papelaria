import styled from 'styled-components';

export const Container = styled.div `
    display: flex;
    width: 150px;
    justify-content: space-around;
    align-items: center;
    margin-right: 1%;
`