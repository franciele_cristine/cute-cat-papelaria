import Links from '../../atoms/links';
import Icon from '../../atoms/icons';
import {TiShoppingCart} from 'react-icons/ti';
import {Container} from './styled';
import {useSelector} from 'react-redux';

const CartLink = () => {
    const cart = useSelector(state => state.cart);
    const total = cart.reduce((acc, product) => acc + product.qtd, 0)
    return(
        <Container>
            <Icon><TiShoppingCart/></Icon>
            <Links name={`Meu Carrinho (${total})`} link={'/cart'}/>
        </Container>
    )
}

export default CartLink