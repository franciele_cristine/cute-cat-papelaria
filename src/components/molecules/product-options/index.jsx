import Selector from '../selector';
import Input from '../../atoms/inputs';
import {Container} from './style';

const ProductOptions = ({options}) =>{
    return(
        <Container>
            <Selector options={options} />
            <Input type={"number"}/>
        </Container>
    )
}

export default ProductOptions