import styled from 'styled-components';

export const Container = styled.div `
    display: flex;
    justify-content: space-between;
    width: 40vw;
    color: #707070;
`