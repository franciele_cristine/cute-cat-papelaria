import Icon from '../../atoms/icons';
import Links from '../../atoms/links';
import {RiUserSharedLine} from 'react-icons/ri';
import {Container} from './style';

const LoginLink = () =>{
    return(
        <Container>
            <Icon><RiUserSharedLine/></Icon>
            <Links name={"Login"} link={"/login"}/>
        </Container>
    )
}

export default LoginLink