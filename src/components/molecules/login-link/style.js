import styled from 'styled-components'

export const Container = styled.div `
    width: 70px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    margin-left: 1%;
`