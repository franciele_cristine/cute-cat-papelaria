import {Container} from './style';
import Button from '../../atoms/button';
import ProductPrice from '../../atoms/product-price';

const Information = ({price, size, onClick}) =>{
    return(
    <Container>
        <ProductPrice>{price}</ProductPrice>
        <Button onClick={onClick}size={size}>Comprar</Button>
    </Container>
    )
}

export default Information