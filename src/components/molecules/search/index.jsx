import Inputs from '../../atoms/inputs';
import Icon from '../../atoms/icons';
import {AiOutlineSearch} from 'react-icons/ai';
import {Container} from './style';

const Search = () =>{
    return(
        <Container>
            <Inputs type={"search"} placeholder={"Busca"}/>
            <Icon><AiOutlineSearch/></Icon>
        </Container>
    )
}

export default Search