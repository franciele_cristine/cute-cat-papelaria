import Routes from './helpers/routes';
import Header from './components/organisms/header';
import Menu from './components/organisms/menu';
import Slide from './components/organisms/slide';

function App() {
  return (
    <div className="App">
      <Header/>
      <Slide/>
      <Menu/>
      <Routes/>
    </div>
  );
}

export default App;
