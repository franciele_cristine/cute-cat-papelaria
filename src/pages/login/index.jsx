import Login from '../../components/organisms/login';
import FormTitle from '../../components/atoms/form-title';
import {
    Container,
    Col1,
    Col2,
} from './style';

const LoginPage = () =>{
    return(
        <Container>
            <Col1>
                <FormTitle title="Login"/>
                <Login/>
            </Col1>
            <Col2>

            </Col2>
        </Container>  
    )
}
export default LoginPage