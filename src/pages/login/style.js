import styled from 'styled-components';

export const Container = styled.div `
    display: flex;
    width: 84vw;
    justify-content: space-around;
`
export const Col1 = styled.div `
    width: 38vw;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 50vh;
    margin-top: 2%;
`
export const Col2 = styled(Col1) `

`