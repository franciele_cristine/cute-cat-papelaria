import {useSelector} from 'react-redux';
import Container from  '../../components/organisms/container';
import Card from '../../components/molecules/card';
import {useParams} from 'react-router-dom';

const Categories = () =>{
    const {category} = useParams();
    const products = useSelector(state => state.products)
    const filtered = products.filter((item) => item.category === category.toLowerCase())

    return(
        <>
        <h1>{category}</h1>
        <Container>
            {filtered.map((product, index) =>{
                return(
                    <Card product={product} key={index}/>
                )
            })}
        </Container>
        </>
    )
}

export default Categories