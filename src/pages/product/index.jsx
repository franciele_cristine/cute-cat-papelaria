import Product from '../../components/organisms/product';
import {useParams} from 'react-router-dom';
import {useSelector} from 'react-redux';

const ProductPage = () =>{
    const {product} = useParams();
    const products = useSelector(state => state.products);
    const selectedProduct = products.find((item) => item.name === product)

    return(
        <Product product={selectedProduct} options={selectedProduct.variations}/>       
        
    )
}

export default ProductPage