import {useSelector} from 'react-redux';
import Container from  '../../components/organisms/container';
import Card from '../../components/molecules/card';

const Home = () =>{
    const products = useSelector(state => state.products)
    
    return(
        <>
        <h1>Todos os Produtos</h1>
        <Container>
            
            { products.map((product, index) =>{
            return(
                    <Card product={product} key={index}/>
                )
            })
            }
        </Container>
        </>
    )
}

export default Home