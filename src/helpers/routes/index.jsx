import {Switch, Route} from 'react-router-dom';
import Home from '../../pages/home';
import ProductPage from '../../pages/product';
import LoginPage from '../../pages/login';
import RegisterPage from '../../pages/register';
import CartPage from '../../pages/cart';
import Categories from '../../pages/categories';

const Routes = () =>{
    return(
        <Switch>
            
            <Route path="/products/:product"><ProductPage/></Route>
            <Route path="/profile/:user">Profile</Route>
            <Route path="/register"><RegisterPage/></Route>
            <Route path="/login"><LoginPage/></Route>
            <Route path="/checkout">Checkout</Route>
            <Route path="/cart"><CartPage/></Route>
            <Route path="/:category"><Categories/></Route>
            <Route path="/"><Home/></Route>
        </Switch>
    )
}

export default Routes