import axios from 'axios';

const token = localStorage.getItem("authToken");

const api = axios.create({
    baseURL: "https://papelaria.herokuapp.com/",
    headers:{
        Authorization: `Bearer ${token}`
    }
})

export default api