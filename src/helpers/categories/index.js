export const categories = [
    'Lápis de Cor',
    'Lápis',
    'Borrachas',
    'Colas',
    'Tesouras',
    'Canetas',
    'Cadernos',
    'Mochilas',
    'Estojos',
    'Marca Textos',
]